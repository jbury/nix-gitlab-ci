{
  outputs = {
    self,
    flake-parts,
    systems,
    nixpkgs,
    devenv,
    ...
  } @ inputs:
    flake-parts.lib.mkFlake {inherit inputs;} {
      imports = [
        inputs.devenv.flakeModule
        ./flakeModule.nix
      ];
      systems = import systems;
      flake = {
        imports = [
          ./utils.nix
        ];
        flakeModule = ./flakeModule.nix;
      };
      perSystem = {
        pkgs,
        system,
        inputs',
        ...
      }: {
        formatter = pkgs.alejandra;
        devenv.shells = {
          default = {
            packages = [pkgs.dive pkgs.skopeo];

            pre-commit = {
              hooks = {
                alejandra.enable = true;
              };
            };
          };
        };
        ci = {
          stages = ["test"];
          default = {
            retry = {
              max = 2;
              when = "runner_system_failure";
            };
          };
          jobs = {
            "test" = {
              stage = "test";
              # wait an hour so the image builds
              when = "delayed";
              start_in = "1 hour";
              deps = [pkgs.hello pkgs.curl];
              script = [
                "hello"
                "curl google.de"
              ];
            };
            "test-non-nix" = {
              nix = false;
              stage = "test";
              image = "alpine:latest";
              script = [
                "echo \"This job will not be modified to use nix\""
              ];
            };
          };
        };

        packages = {
          image = pkgs.dockerTools.buildImage {
            name = "nix-gitlab-ci";
            fromImage = pkgs.dockerTools.pullImage {
              imageName = "nixpkgs/nix-flakes";
              imageDigest = "sha256:d88e521662cb6bf9cef006b79ed6ed1069e297171f3c2585f2b898b30f7c045c";
              sha256 = "1pcbgxz9c98mfqrzyi14h568dw8vxj1kbgirnwl6vs8wfaamjaaf";
              finalImageName = "nixpkgs/nix-flakes";
              finalImageTag = "latest";
            };
            copyToRoot = pkgs.buildEnv {
              name = "image-root";
              paths = [pkgs.gitMinimal pkgs.cachix inputs'.attic.packages.attic-client];
              pathsToLink = ["/bin"];
            };
          };
        };
      };
    };

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";

    # flake & devenv related
    flake-parts.url = "github:hercules-ci/flake-parts";
    systems.url = "github:nix-systems/default-linux";
    devenv = {
      url = "github:cachix/devenv";
      inputs.pre-commit-hooks.follows = "pre-commit-hooks";
    };
    pre-commit-hooks.url = "github:cachix/pre-commit-hooks.nix";
    attic.url = "github:zhaofengli/attic";
  };

  nixConfig = {
    extra-substituters = [
      "https://cache.nixos.org/"
      "https://nix-community.cachix.org"
    ];

    extra-trusted-public-keys = [
      "cache.nixos.org-1:6NCHdD59X431o0gWypbMrAURkbJ16ZPMQFGspcDShjY="
      "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
    ];
  };
}
