# Nix Gitlab CI

Flake module which allows generating a `.gitlab-ci.yml` from Nix.
This allows easily using any Nix package in CI.
Also makes it possible to split CI parts in a separate module 
which can be imported in multiple projects.


## Usage
```nix
# flake.nix
{
  ...
  inputs.nix-gitlab-ci.url = "gitlab:TECHNOFAB/nix-gitlab-ci";
  
  outputs = {...}: flake-parts.lib.mkFlake {...} {
    imports = [
      inputs.nix-gitlab-ci.flakeModule
    ];
    ...
    
    perSystem = {...}: {
      ci = {
        stages = ["test"];
        jobs = {
          "test" = {
            stage = "test";
            deps = [pkgs.unixtools.ping];
            script = [
              "ping -c 5 8.8.8.8"
            ];
          };
        };
      };
      ...
    }
  }
}
```
```yaml
# .gitlab-ci.yml
include:
  - project: TECHNOFAB/nix-gitlab-ci
    ref: main
    file: gitlab-ci.yml
```
