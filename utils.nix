{...}: {
  mkUtils = {pkgs, ...}: {
    commitAndPushFiles = {
      message,
      files ? [],
    }: jobArgs:
      jobArgs
      // {
        before_script = [
          ''
            eval "$(ssh-agent -s)" >/dev/null;
            ssh-keyscan -t rsa $CI_SERVER_HOST >> ~/.ssh/known_hosts;
            echo "$GIT_SSH_PRIV_KEY" | tr -d '\r' | ssh-add - >/dev/null;
            git config --global user.email "$GIT_EMAIL" >/dev/null;
            git config --global user.name "$GIT_NAME" >/dev/null;
            export CI_PUSH_REPO=`echo $CI_REPOSITORY_URL | sed -e "s|.*@\(.*\)|git@\1|" -e "s|/|:|"`;
            git remote rm origin && git remote add origin ''${CI_PUSH_REPO}
          ''
        ];
        script =
          (jobArgs.script or [])
          ++ (
            if builtins.length files == 0
            then []
            else [
              ''
                git add ${builtins.concatStringsSep " " files}
              ''
            ]
          )
          ++ [
            ''
              git diff --cached --exit-code >/dev/null &&
                echo "Nothing to commit" ||
                git commit -m "${message}" --no-verify;
              git push --tags origin ''${GIT_SOURCE_REF:-HEAD}:''${GIT_TARGET_REF:-$CI_COMMIT_REF_NAME} -o ci.skip
            ''
          ];
        deps = (jobArgs.deps or []) ++ [pkgs.openssh pkgs.gitMinimal pkgs.gnused];
      };
  };
}
